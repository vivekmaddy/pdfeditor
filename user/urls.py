from django.urls import path
from . import views

app_name = "user"
urlpatterns = [
    path('login/', views.auth, name="auth"),
    path('', views.home, name="home"),
    path('sign_document/', views.sign_document, name="sign_document"),
    path('submit_pdf_form/', views.submit_pdf_form, name="submit_pdf_form"),
]
