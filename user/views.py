from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.contrib import messages

from user.models import *


# Create your views here.


def auth(request):
    if request.method == "POST":
        try:
            username, password = request.POST["username"], request.POST["password"]
            user = authenticate(username=username, password=password)
            if not user:
                raise Exception("Invalid username or password")
            login(request, user)
            messages.success(request, "Login Successful")
            return redirect("user:home")

        except Exception as err:
            messages.error(request, str(err), extra_tags="danger")

    return render(request, "users/auth.html", {})


@login_required
def home(request):
    return render(request, "users/home.html", {"user_details": request.user.userdetails})


@login_required
def sign_document(request):
    user_details = request.user.userdetails
    initial_doc = user_details.document_template if not user_details.document else user_details.document
    return render(request, "users/sign_document.html", {"initialDoc": initial_doc})


@csrf_exempt
@login_required
def submit_pdf_form(request):
    is_ajax = request.headers.get('X-Requested-With') == 'XMLHttpRequest'

    if is_ajax and request.method == "POST":
        response = {
            "status": 201,
            "message": "Created"
        }
        try:
            document = request.FILES["file"]
            user_details = UserDetails.objects.get(user=request.user)
            user_details.document = document
            user_details.save()
        except Exception as err:
            print(err)
            response["message"] = str(err)
            response["status"] = 400

        return JsonResponse(response, status=response["status"])
    else:
        return HttpResponseBadRequest('Invalid request')
