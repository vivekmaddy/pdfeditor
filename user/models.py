from django.contrib.auth.models import User
from django.db import models
from django.core.exceptions import ValidationError
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.

class Base(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


def extension_validate(value):
    if not value.name.endswith(".pdf"):
        raise ValidationError("Invalid File Format!")


class SamplePDFCollections(Base):
    file = models.FileField(upload_to='sample_pdf_collections', validators=[extension_validate])


def get_user_details_document_template_path(instance, file_name):
    return f"Document/{instance.user.username}/DocumentTemplate/{file_name}"


def get_user_details_document_path(instance, file_name):
    return f"Document/{instance.user.username}/{file_name}"


class UserDetails(Base):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    document_template = models.FileField(upload_to=get_user_details_document_template_path)
    document = models.FileField(upload_to=get_user_details_document_path, blank=True, null=True)
    is_verified = models.BooleanField(default=False)


# signals
@receiver(post_save, sender=User, dispatch_uid="create_user_details")
def create_user_details(sender, instance, **kwargs):
    if not UserDetails.objects.filter(user=instance).exists():
        latest_pdf_template = SamplePDFCollections.objects.last().file
        UserDetails.objects.create(
            user=instance,
            document_template=latest_pdf_template,
        )
